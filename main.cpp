
#include <shader_manager/shader_manager.h>
#include <core_manager.h>
#include <hand_tracking/hand_tracker.h>
#include <hand_tracking/kinect_tracker.h>


#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <Python.h>

#include <cmath>
#include <chrono>
#include <thread>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>

// #include <Kinect.h>

int main()
{

     int screenWidth = 1280, screenHeight = 720;//1920x1080
                                                //  int screenWidth = 1280, screenHeight=720 ;
     touch_symphony::CoreManager& manager = touch_symphony::CoreManager::GetCoreManager();
     auto tracker = manager.GetKinectTracker( screenWidth, screenHeight );
     auto pp = manager.GetSlimeMolding( screenWidth, screenHeight );
     //  auto pp = manager.GetPseudoParticles(screenWidth, screenHeight);
     if( pp.get()->Init() > 0 )
     {
          std::cerr << "PseudoParticles is not init!" << std::endl;
          return -1;
     }

     if( !tracker.get()->initialize() )
     {
          std::cerr << "KinectTracker is not init!" << std::endl;
          return -1;
     }
     int frame = 0;
     double clock;
     double xpos, ypos;
     int width, height;
     int mod = 0;
     double lastIncrementTime = glfwGetTime();


     // Рендеринг цикл
     while( !glfwWindowShouldClose( pp.get()->getWindow() ) )
     {
          // Очистка экрана
          glClear( GL_COLOR_BUFFER_BIT );

          //frameCounter++;
          double currentTime = glfwGetTime();
          double elapsedTime = currentTime - lastIncrementTime;
          if( elapsedTime >= 4.0 )
          {

               mod++;// Увеличиваем mod
               if( mod > 2 )
               {
                    mod = 0;
               }
               lastIncrementTime = currentTime;// Обновляем время последнего увеличения
          }

          clock = glfwGetTime();

          //glfwGetCursorPos(pp.get()->getWindow(), &xpos, &ypos);

          auto skeletInfo = tracker.get()->getHands();

          // skeletInfo[0].lHand.first;
          // skeletInfo[0].lHand.second;
          // skeletInfo[0].rHand.first;
          // skeletInfo[0].lHand.second;
          //pp.get()->Draw(skeletInfo, frame);


          pp.get()->Draw( skeletInfo, clock, mod );
          //  frame++;
          std::this_thread::sleep_for( std::chrono::milliseconds( 16 ) );//60fps//16
     }


     // Закрытие GLFW
     glfwTerminate();

     return 0;
}