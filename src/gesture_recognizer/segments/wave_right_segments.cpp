#include "wave_right_segments.h"

GesturePartResult WaveRightSegment1::Update( const Body& body )
{
     if( body.Joints[ JointType_WristRight ].Position.Y > body.Joints[ JointType_ElbowRight ].Position.Y )
     {
          if( body.Joints[ JointType_WristRight ].Position.X > body.Joints[ JointType_ElbowRight ].Position.X )
          {
               return GesturePartResult::Succeeded;
          }
          return GesturePartResult::Undetermined;
     }
     return GesturePartResult::Failed;
}

GesturePartResult WaveRightSegment2::Update( const Body& body )
{
     if( body.Joints[ JointType_WristRight ].Position.Y > body.Joints[ JointType_ElbowRight ].Position.Y )
     {
          if( body.Joints[ JointType_WristRight ].Position.X < body.Joints[ JointType_ElbowRight ].Position.X )
          {
               return GesturePartResult::Succeeded;
          }
          return GesturePartResult::Undetermined;
     }
     return GesturePartResult::Failed;
}
