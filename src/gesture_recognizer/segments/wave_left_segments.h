#pragma once

#include "../kinect_types.h"

class WaveLeftSegment1 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};

class WaveLeftSegment2 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};
