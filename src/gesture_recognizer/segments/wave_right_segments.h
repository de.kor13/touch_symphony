#ifndef WAVERIGHTSEGMENT_H
#define WAVERIGHTSEGMENT_H

#include "../kinect_types.h"

class WaveRightSegment1 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};

class WaveRightSegment2 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};
#endif// WAVERIGHTSEGMENT_H