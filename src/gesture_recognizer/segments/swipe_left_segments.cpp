#include "swipe_left_segments.h"


GesturePartResult SwipeLeftSegment1::Update( const Body& body )
{
     if( body.Joints[ JointType_HandRight ].Position.Z < body.Joints[ JointType_ElbowRight ].Position.Z &&
         body.Joints[ JointType_HandLeft ].Position.Y < body.Joints[ JointType_SpineShoulder ].Position.Y )
     {

          if( body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_Head ].Position.Y &&
              body.Joints[ JointType_HandRight ].Position.Y > body.Joints[ JointType_SpineBase ].Position.Y )
          {

               if( body.Joints[ JointType_HandRight ].Position.X > body.Joints[ JointType_ShoulderRight ].Position.X )
               {
                    return GesturePartResult::Succeeded;
               }
               return GesturePartResult::Undetermined;
          }
          return GesturePartResult::Failed;
     }
     return GesturePartResult::Failed;
}

GesturePartResult SwipeLeftSegment2::Update( const Body& body )
{
     if( body.Joints[ JointType_HandRight ].Position.Z < body.Joints[ JointType_ElbowRight ].Position.Z &&
         body.Joints[ JointType_HandLeft ].Position.Y < body.Joints[ JointType_SpineShoulder ].Position.Y )
     {

          if( body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_Head ].Position.Y &&
              body.Joints[ JointType_HandRight ].Position.Y > body.Joints[ JointType_SpineBase ].Position.Y )
          {

               if( body.Joints[ JointType_HandRight ].Position.X < body.Joints[ JointType_ShoulderRight ].Position.X &&
                   body.Joints[ JointType_HandRight ].Position.X > body.Joints[ JointType_ShoulderLeft ].Position.X )
               {
                    return GesturePartResult::Succeeded;
               }
               return GesturePartResult::Undetermined;
          }
          return GesturePartResult::Failed;
     }
     return GesturePartResult::Failed;
}

GesturePartResult SwipeLeftSegment3::Update( const Body& body )
{
     if( body.Joints[ JointType_HandRight ].Position.Z < body.Joints[ JointType_ElbowRight ].Position.Z &&
         body.Joints[ JointType_HandLeft ].Position.Y < body.Joints[ JointType_SpineShoulder ].Position.Y )
     {

          if( body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_SpineShoulder ].Position.Y &&
              body.Joints[ JointType_HandRight ].Position.Y > body.Joints[ JointType_SpineBase ].Position.Y )
          {

               if( body.Joints[ JointType_HandRight ].Position.X < body.Joints[ JointType_ShoulderLeft ].Position.X )
               {
                    return GesturePartResult::Succeeded;
               }
               return GesturePartResult::Undetermined;
          }

          return GesturePartResult::Failed;
     }

     return GesturePartResult::Failed;
}
