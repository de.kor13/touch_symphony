#ifndef GESTURE_SEGMENTS_H
#define GESTURE_SEGMENTS_H

#include "../kinect_types.h"

class SwipeLeftSegment1 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};

class SwipeLeftSegment2 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};

class SwipeLeftSegment3 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};


#endif// GESTURE_SEGMENTS_H