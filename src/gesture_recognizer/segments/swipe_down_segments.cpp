#include "swipe_down_segments.h"

GesturePartResult SwipeDownSegment1::Update( const Body& body )
{
     if( body.Joints[ JointType_HandRight ].Position.Z < body.Joints[ JointType_ElbowRight ].Position.Z &&
         body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_SpineShoulder ].Position.Y )
     {

          if( body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_Head ].Position.Y &&
              body.Joints[ JointType_HandRight ].Position.Y > body.Joints[ JointType_ElbowRight ].Position.Y )
          {

               if( body.Joints[ JointType_HandRight ].Position.X > body.Joints[ JointType_ShoulderRight ].Position.X )
               {
                    return GesturePartResult::Succeeded;
               }
               return GesturePartResult::Undetermined;
          }
          return GesturePartResult::Failed;
     }
     return GesturePartResult::Failed;
}

GesturePartResult SwipeDownSegment2::Update( const Body& body )
{
     if( body.Joints[ JointType_HandRight ].Position.Z < body.Joints[ JointType_ElbowRight ].Position.Z &&
         body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_SpineShoulder ].Position.Y )
     {

          if( body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_ElbowRight ].Position.Y )
          {

               if( body.Joints[ JointType_HandRight ].Position.X > body.Joints[ JointType_HipRight ].Position.X )
               {
                    return GesturePartResult::Succeeded;
               }
               return GesturePartResult::Undetermined;
          }
          return GesturePartResult::Failed;
     }
     return GesturePartResult::Failed;
}

GesturePartResult SwipeDownSegment3::Update( const Body& body )
{
     if( body.Joints[ JointType_HandRight ].Position.Z < body.Joints[ JointType_ElbowRight ].Position.Z &&
         body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_SpineShoulder ].Position.Y )
     {

          if( body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_HipRight ].Position.Y )
          {

               if( body.Joints[ JointType_HandRight ].Position.X > body.Joints[ JointType_HipRight ].Position.X )
               {
                    return GesturePartResult::Succeeded;
               }
               return GesturePartResult::Undetermined;
          }
          return GesturePartResult::Failed;
     }
     return GesturePartResult::Failed;
}
