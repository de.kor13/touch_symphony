#include "swipe_up_segments.h"

GesturePartResult SwipeUpSegment1::Update(const Body& body)
{
    if (body.Joints[JointType_HandRight].Position.Z < body.Joints[JointType_ElbowRight].Position.Z)
    {
        if (body.Joints[JointType_HandRight].Position.Y < body.Joints[JointType_Head].Position.Y &&
            body.Joints[JointType_HandRight].Position.Y > body.Joints[JointType_SpineBase].Position.Y)
        {
            if (body.Joints[JointType_HandRight].Position.X > body.Joints[JointType_ShoulderRight].Position.X)
            {
                return GesturePartResult::Succeeded;
            }
            return GesturePartResult::Undetermined;
        }
        return GesturePartResult::Failed;
    }
    return GesturePartResult::Failed;
}

GesturePartResult SwipeUpSegment2::Update(const Body& body)
{
    if (body.Joints[JointType_HandRight].Position.Z < body.Joints[JointType_ShoulderRight].Position.Z)
    {
        if (body.Joints[JointType_HandRight].Position.Y > body.Joints[JointType_ShoulderRight].Position.Y)
        {
            if (body.Joints[JointType_HandRight].Position.X > body.Joints[JointType_ShoulderRight].Position.X)
            {
                return GesturePartResult::Succeeded;
            }
            return GesturePartResult::Undetermined;
        }
        return GesturePartResult::Failed;
    }
    return GesturePartResult::Failed;
}

GesturePartResult SwipeUpSegment3::Update(const Body& body)
{
    if (body.Joints[JointType_HandRight].Position.Z < body.Joints[JointType_ShoulderRight].Position.Z)
    {
        if (body.Joints[JointType_HandRight].Position.Y > body.Joints[JointType_Head].Position.Y)
        {
            if (body.Joints[JointType_HandRight].Position.X > body.Joints[JointType_ShoulderRight].Position.X)
            {
                return GesturePartResult::Succeeded;
            }
            return GesturePartResult::Undetermined;
        }
        return GesturePartResult::Failed;
    }
    return GesturePartResult::Failed;
}
