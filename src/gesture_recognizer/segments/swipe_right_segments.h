#ifndef SWIPE_RIGHT_SEGMENTS_H
#define SWIPE_RIGHT_SEGMENTS_H

#include "../kinect_types.h"


class SwipeRightSegment1 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};

class SwipeRightSegment2 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};

class SwipeRightSegment3 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};


#endif// SWIPE_RIGHT_SEGMENTS_H