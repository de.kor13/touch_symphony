#ifndef SWIPE_UP_SEGMENTS_H
#define SWIPE_UP_SEGMENTS_H

#include "../kinect_types.h"

class SwipeUpSegment1 : public IGestureSegment
{
public:
    GesturePartResult Update(const Body& body) override;
};

class SwipeUpSegment2 : public IGestureSegment
{
public:
    GesturePartResult Update(const Body& body) override;
};

class SwipeUpSegment3 : public IGestureSegment
{
public:
    GesturePartResult Update(const Body& body) override;
};

#endif // SWIPE_UP_SEGMENTS_H