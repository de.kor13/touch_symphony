#include "swipe_right_segments.h"


GesturePartResult SwipeRightSegment1::Update( const Body& body )
{
     if( body.Joints[ JointType_HandLeft ].Position.Z < body.Joints[ JointType_ElbowLeft ].Position.Z &&
         body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_SpineBase ].Position.Y )
     {

          if( body.Joints[ JointType_HandLeft ].Position.Y < body.Joints[ JointType_Head ].Position.Y &&
              body.Joints[ JointType_HandLeft ].Position.Y > body.Joints[ JointType_SpineBase ].Position.Y )
          {

               if( body.Joints[ JointType_HandLeft ].Position.X < body.Joints[ JointType_ShoulderLeft ].Position.X )
               {
                    return GesturePartResult::Succeeded;
               }

               return GesturePartResult::Undetermined;
          }

          return GesturePartResult::Failed;
     }

     return GesturePartResult::Failed;
}

GesturePartResult SwipeRightSegment2::Update( const Body& body )
{
     if( body.Joints[ JointType_HandLeft ].Position.Z < body.Joints[ JointType_ElbowLeft ].Position.Z &&
         body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_SpineBase ].Position.Y )
     {

          if( body.Joints[ JointType_HandLeft ].Position.Y < body.Joints[ JointType_Head ].Position.Y &&
              body.Joints[ JointType_HandLeft ].Position.Y > body.Joints[ JointType_SpineBase ].Position.Y )
          {

               if( body.Joints[ JointType_HandLeft ].Position.X < body.Joints[ JointType_ShoulderRight ].Position.X &&
                   body.Joints[ JointType_HandLeft ].Position.X > body.Joints[ JointType_ShoulderLeft ].Position.X )
               {
                    return GesturePartResult::Succeeded;
               }

               return GesturePartResult::Undetermined;
          }

          return GesturePartResult::Failed;
     }

     return GesturePartResult::Failed;
}

GesturePartResult SwipeRightSegment3::Update( const Body& body )
{
     if( body.Joints[ JointType_HandLeft ].Position.Z < body.Joints[ JointType_ElbowLeft ].Position.Z &&
         body.Joints[ JointType_HandRight ].Position.Y < body.Joints[ JointType_SpineBase ].Position.Y )
     {

          if( body.Joints[ JointType_HandLeft ].Position.Y < body.Joints[ JointType_Head ].Position.Y &&
              body.Joints[ JointType_HandLeft ].Position.Y > body.Joints[ JointType_SpineBase ].Position.Y )
          {

               if( body.Joints[ JointType_HandLeft ].Position.X > body.Joints[ JointType_ShoulderRight ].Position.X )
               {
                    return GesturePartResult::Succeeded;
               }

               return GesturePartResult::Undetermined;
          }

          return GesturePartResult::Failed;
     }

     return GesturePartResult::Failed;
}
