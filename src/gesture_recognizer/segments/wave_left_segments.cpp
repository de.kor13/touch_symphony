#include "wave_left_segments.h"

GesturePartResult WaveLeftSegment1::Update( const Body& body )
{
     if( body.Joints[ JointType_HandLeft ].Position.Y > body.Joints[ JointType_ElbowLeft ].Position.Y )
     {
          if( body.Joints[ JointType_HandLeft ].Position.X > body.Joints[ JointType_ElbowLeft ].Position.X )
          {
               return GesturePartResult::Succeeded;
          }
          return GesturePartResult::Undetermined;
     }
     return GesturePartResult::Failed;
}

GesturePartResult WaveLeftSegment2::Update( const Body& body )
{
     if( body.Joints[ JointType_HandLeft ].Position.Y > body.Joints[ JointType_ElbowLeft ].Position.Y )
     {
          if( body.Joints[ JointType_HandLeft ].Position.X < body.Joints[ JointType_ElbowLeft ].Position.X )
          {
               return GesturePartResult::Succeeded;
          }
          return GesturePartResult::Undetermined;
     }
     return GesturePartResult::Failed;
}
