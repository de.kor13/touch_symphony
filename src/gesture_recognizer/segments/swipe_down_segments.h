#ifndef SWIPE_DOWN_SEGMENTS_H
#define SWIPE_DOWN_SEGMENTS_H

#include "../kinect_types.h"

class SwipeDownSegment1 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};

class SwipeDownSegment2 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};

class SwipeDownSegment3 : public IGestureSegment
{
public:
     GesturePartResult Update( const Body& body ) override;
};


#endif// SWIPE_DOWN_SEGMENTS_H