#ifndef GESTURE_H
#define GESTURE_H

#include <vector>
#include <memory>
#include <functional>
#include <unordered_map>

#include "kinect_types.h"


class Gesture
{
public:
     Gesture()
     {
     }
     Gesture(GestureType type, std::vector<std::unique_ptr<IGestureSegment>> segments);

     GestureType GestureT;
     std::vector<std::unique_ptr<IGestureSegment>> Segments;

     GesturePartResult Update( const Body& body );
     void Reset();

     std::function< void( Gesture*, GestureEventArgs ) > GestureRecognized;

private:
     int WINDOW_SIZE = 50;
     int _currentSegment = 0;
     int _pausedFrameCount = 10;
     int _frameCount = 0;
     bool _isPaused = false;
};

#endif// GESTURE_H