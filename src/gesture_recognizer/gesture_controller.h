#ifndef GESTURE_CONTROLLER_H
#define GESTURE_CONTROLLER_H

#include <vector>
#include <memory>
#include <unordered_map>
#include <functional>

#include "kinect_types.h"
#include "gesture.h"


class GestureController
{
public:
     GestureController();
     GestureController( GestureType type );

     void AddGesture( GestureType type );
     void Update( const Body& body );

     std::function< void( GestureController*, GestureEventArgs ) > GestureRecognized;

private:
     std::unordered_map< GestureType, std::unique_ptr< Gesture > > _gestures;
};

#endif// GESTURE_CONTROLLER_H
