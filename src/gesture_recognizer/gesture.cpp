#include "gesture.h"

Gesture::Gesture( GestureType type, std::vector< std::unique_ptr< IGestureSegment > > segments )
    : GestureT( type ), Segments( std::move( segments ) )
{
}

GesturePartResult Gesture::Update( const Body& body )
{
     if( _isPaused )
     {
          if( _frameCount == _pausedFrameCount )
          {
               _isPaused = false;
          }
          _frameCount++;
     }

     GesturePartResult result = Segments[ _currentSegment ]->Update( body );

     if( result == GesturePartResult::Succeeded )
     {
          if( _currentSegment + 1 < Segments.size() )
          {
               _currentSegment++;
               _frameCount = 0;
               _pausedFrameCount = 10;
               _isPaused = true;
          }
          else
          {
               if( GestureRecognized != nullptr )
               {
                    GestureRecognized( this, GestureEventArgs( GestureT, body.TrackingId ) );
                    Reset();
               }
          }
     }
     else if( result == GesturePartResult::Failed || _frameCount == WINDOW_SIZE )
     {
          Reset();
     }
     else
     {
          _frameCount++;
          _pausedFrameCount = 10 / 2;
          _isPaused = true;
     }

     return result;
}

void Gesture::Reset()
{
     _currentSegment = 0;
     _frameCount = 0;
     _pausedFrameCount = 10 / 2;
     _isPaused = true;
}
