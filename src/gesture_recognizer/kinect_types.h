#ifndef KINECT_TYPES_H
#define KINECT_TYPES_H

#include <unordered_map>
#include <utility>
#include <Kinect.h>

// Определение перечисления типов жестов
enum class GestureType
{
     JoinedHands,
     SwipeDown,
     SwipeLeft,
     SwipeRight,
     SwipeUp,
     WaveRight,
     WaveLeft,
     ZoomIn,
     ZoomOut
};
// Определение перечисления результатов сегментов жестов
enum class GesturePartResult
{
     Succeeded,
     Failed,
     Undetermined
};

// Определение структуры тела
struct Body
{
     Joint Joints[ JointType_Count ];
     unsigned long long TrackingId;
};

// Определение абстрактного класса для сегментов жестов
class IGestureSegment
{
public:
     virtual GesturePartResult Update( const Body& body ) = 0;
};

// Определение класса для аргументов события жеста
class GestureEventArgs
{
public:
     GestureEventArgs()
     {
     }
     GestureEventArgs( GestureType type, unsigned long trackingID ) : GestureType( type ), TrackingID( trackingID )
     {
     }

     GestureType GestureType;
     unsigned long TrackingID;
};


#endif// KINECT_TYPES_H
