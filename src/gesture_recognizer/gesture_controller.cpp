#include "gesture_controller.h"
#include "segments/wave_left_segments.h"
#include "segments/wave_right_segments.h"
#include "segments/swipe_up_segments.h"
#include "segments/swipe_down_segments.h"
#include "segments/swipe_left_segments.h"
#include "segments/swipe_right_segments.h"

#include <memory>

GestureController::GestureController()
{
     for( int i = 0; i < static_cast< int >( GestureType::ZoomOut ) + 1; i++ )
     {
          AddGesture( static_cast< GestureType >( i ) );
     }
}

GestureController::GestureController( GestureType type )
{
     AddGesture( type );
}

void GestureController::AddGesture( GestureType type )
{
     if( _gestures.find( type ) != _gestures.end() )
          return;

     std::vector< std::unique_ptr< IGestureSegment > > segments;

     switch( type )
     {
          case GestureType::WaveLeft:
          {
               for( int i = 0; i < 3; i++ )
               {
                    segments.push_back( std::make_unique< WaveLeftSegment1 >() );
                    segments.push_back( std::make_unique< WaveLeftSegment2 >() );
               }
               break;
          }
          case GestureType::WaveRight:
          {
               for( int i = 0; i < 3; i++ )
               {
                    segments.push_back( std::make_unique< WaveRightSegment1 >() );
                    segments.push_back( std::make_unique< WaveRightSegment2 >() );
               }
               break;
          }
          case GestureType::SwipeDown:
                    segments.push_back( std::make_unique< SwipeDownSegment1 >() );
                    segments.push_back( std::make_unique< SwipeDownSegment2 >() );
                    segments.push_back( std::make_unique< SwipeDownSegment3 >() );
               break;
          case GestureType::SwipeLeft:
                    segments.push_back( std::make_unique< SwipeLeftSegment1 >() );
                    segments.push_back( std::make_unique< SwipeLeftSegment2 >() );
                    segments.push_back( std::make_unique< SwipeLeftSegment3 >() );
        
               break;
          case GestureType::SwipeRight:

                    segments.push_back( std::make_unique< SwipeRightSegment1 >() );
                    segments.push_back( std::make_unique< SwipeRightSegment2 >() );
                    segments.push_back( std::make_unique< SwipeRightSegment3 >() );
          
               break;
          case GestureType::SwipeUp:

                    segments.push_back( std::make_unique< SwipeUpSegment1 >() );
                    segments.push_back( std::make_unique< SwipeUpSegment2 >() );
                    segments.push_back( std::make_unique< SwipeUpSegment3 >() );
             
               break;
          default:
               break;
     }

     if( !segments.empty() )
     {
          auto gesture = std::make_unique< Gesture >( type, std::move( segments ) );
          gesture->GestureRecognized = [ this ]( Gesture* sender, GestureEventArgs e )
          {
               if( GestureRecognized != nullptr )
               {
                    GestureRecognized( this, e );
               }
          };

          _gestures[ type ] = std::move( gesture );
     }
}

void GestureController::Update( const Body& body )
{
     for( const auto& pair : _gestures )
     {
          pair.second->Update( body );
     }
}