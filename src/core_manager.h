#ifndef CORE_MANAGER_H
#define CORE_MANAGER_H

#include <effects/fluid/fluid_sim_gas.h>
#include <effects/fluid/pseudo_particles.h>
#include <effects/slime/slime_molding.h>
#include <hand_tracking/hand_tracker.h>
#include <hand_tracking/kinect_tracker.h>


#include <memory>
#include <vector>

namespace touch_symphony
{

class CoreManager
{
public:
     static CoreManager& GetCoreManager();

     CoreManager( const CoreManager& ) = delete;
     void operator=( const CoreManager& ) = delete;

     std::shared_ptr< PseudoParticles > GetPseudoParticles( int x, int y );
     std::shared_ptr< SlimeMolding > GetSlimeMolding( int x, int y );
     std::shared_ptr< FluidSimGas > GetFluidSimGas( int x, int y );

     std::shared_ptr< HandTracker > GetHandTracker( int x, int y );
     std::shared_ptr< KinectTracker > GetKinectTracker( int x, int y );


private:
     CoreManager() = default;
     std::shared_ptr< PseudoParticles > pseudoParticles = nullptr;
     std::shared_ptr< SlimeMolding > slimeMolding = nullptr;
     std::shared_ptr< FluidSimGas > fluidSimGas = nullptr;

     std::shared_ptr< HandTracker > handTracker = nullptr;
     std::shared_ptr< KinectTracker > kinectTracker = nullptr;
};

}// namespace touch_symphony

#endif//CORE_MANAGER
