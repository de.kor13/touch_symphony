#ifndef SLIME_MOLDING_H
#define SLIME_MOLDING_H

#include <memory>
#include <utility>
#include <vector>

#include <shader_manager/shader_manager.h>
#include <hand_tracking/kinect_tracker.h>

namespace touch_symphony
{


class SlimeMolding
{
private:
     GLuint prog;
     GLuint prog1;
     GLuint prog2;
     GLuint VAO;

     GLuint texture1, framebuffer1;
     GLuint texture2, framebuffer2;
     GLuint texture3, framebuffer3;
     GLuint texture4, framebuffer4;

     int width_, height_;
     std::shared_ptr< ShaderManager > shaderManager = nullptr;

public:
     SlimeMolding( int width, int height );
     ~SlimeMolding(){};
     int Init();
     void Draw( std::vector< SkeletonInfo > vec, float time, int mod );
     GLFWwindow* getWindow();
};

}// namespace touch_symphony

#endif//SLIME_MOLDING
