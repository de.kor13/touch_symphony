#include "slime_molding.h"

#include <filesystem>
#include <iostream>

namespace fs = std::filesystem;
namespace touch_symphony
{


SlimeMolding::SlimeMolding( int width, int height ) : width_( width ), height_( height )
{

     shaderManager = std::make_shared< ShaderManager >( width, height );
}

int SlimeMolding::Init()
{
     std::string shaderFrag = ( fs::current_path() / "shaders" / "slime_molding" / "main.frag" ).string();
     std::string shaderFragBufferA = ( fs::current_path() / "shaders" / "slime_molding" / "buffer_a.frag" ).string();
     std::string shaderFragBufferB = ( fs::current_path() / "shaders" / "slime_molding" / "buffer_b.frag" ).string();
     std::string shaderVertex = ( fs::current_path() / "shaders" / "vertex.vert" ).string();

     prog = shaderManager.get()->CreateShaderProgram( shaderVertex, shaderFrag );
     prog2 = shaderManager.get()->CreateShaderProgram( shaderVertex, shaderFragBufferB );
     prog1 = shaderManager.get()->CreateShaderProgram( shaderVertex, shaderFragBufferA );
     VAO = shaderManager.get()->createVertexArrayObject();

     if( shaderManager.get()->createTextureAndBindFramebuffer( texture1, framebuffer1, width_, height_ ) > 0 )
     {
          std::cerr << "Framebuffer is not complete!" << std::endl;
          return 1;
     }
     if( shaderManager.get()->createTextureAndBindFramebuffer( texture2, framebuffer2, width_, height_ ) > 0 )
     {
          std::cerr << "Framebuffer is not complete!" << std::endl;
          return 1;
     }
     if( shaderManager.get()->createTextureAndBindFramebuffer( texture3, framebuffer3, width_, height_ ) > 0 )
     {
          std::cerr << "Framebuffer is not complete!" << std::endl;
          return 1;
     }
     if( shaderManager.get()->createTextureAndBindFramebuffer( texture4, framebuffer4, width_, height_ ) > 0 )
     {
          std::cerr << "Framebuffer is not complete!" << std::endl;
          return 1;
     }

     return 0;
}

void SlimeMolding::Draw( std::vector< SkeletonInfo > vec, float time , int mod )
{
     glfwGetFramebufferSize( shaderManager.get()->getWindow(), &width_, &height_ );


     shaderManager.get()->UseProgramAndBindFramebuffer( prog2, framebuffer3 );
     shaderManager.get()->ClearBuffers();
     shaderManager.get()->SetTextureUniforms( texture2, texture4 );

     glUniform1i( glGetUniformLocation( prog2, "iChannel0" ), 0 );
     glUniform1i( glGetUniformLocation( prog2, "iChannel1" ), 1 );
     glUniform2f( glGetUniformLocation( prog2, "iResolution" ), width_, height_ );
     glUniform1ui( glGetUniformLocation( prog2, "mod" ), mod );

     shaderManager.get()->DrawElements();

     shaderManager.get()->UseProgramAndBindFramebuffer( prog1, framebuffer1 );
     shaderManager.get()->ClearBuffers();
     shaderManager.get()->SetTextureUniforms( texture2, texture3 );
     
     glUniform1i( glGetUniformLocation( prog1, "iChannel0" ), 0 );
     glUniform1i( glGetUniformLocation( prog1, "iChannel1" ), 1 );
     glUniform2f( glGetUniformLocation( prog1, "iResolution" ), width_, height_ );
     glUniform1f( glGetUniformLocation( prog1, "iTime" ), time );
    
     for (int i = 0; i < vec.size(); ++i) 
          {
               glUniform1f( glGetUniformLocation( prog1, "sLen" ), vec[i].sensorLen );
               glUniform1f( glGetUniformLocation( prog1, "radiusR" ), vec[i].radiusRight );
               glUniform1f( glGetUniformLocation( prog1, "radiusL" ), vec[i].radiusLeft );
               std::string iMouse = "iMouse[" + std::to_string(i) + "]";
               std::string iMouse1 = "iMouse1[" + std::to_string(i) + "]";
               glUniform2f(glGetUniformLocation(prog1, iMouse.c_str()), static_cast<float>(vec[i].rHand.first),
                              static_cast<float>(height_ - vec[i].rHand.second));
               glUniform2f(glGetUniformLocation(prog1, iMouse1.c_str()), static_cast<float>(vec[i].lHand.first),
                              static_cast<float>(height_ - vec[i].lHand.second));
          }
     
     shaderManager.get()->DrawElements();


     shaderManager.get()->UseProgramAndBindFramebuffer( prog1, framebuffer2 );
     shaderManager.get()->ClearBuffers();
     shaderManager.get()->SetTextureUniforms( texture1, texture3 );
     glUniform1i( glGetUniformLocation( prog1, "iChannel0" ), 0 );
     glUniform1i( glGetUniformLocation( prog1, "iChannel1" ), 1 );

     glUniform2f( glGetUniformLocation( prog1, "iResolution" ), width_, height_ );
     glUniform1f( glGetUniformLocation( prog1, "iTime" ), time );
     for (int i = 0; i < vec.size(); ++i) 
     {
          glUniform1f( glGetUniformLocation( prog1, "sLen" ), vec[i].sensorLen );
          glUniform1f( glGetUniformLocation( prog1, "radiusR" ), vec[i].radiusRight );
          glUniform1f( glGetUniformLocation( prog1, "radiusL" ), vec[i].radiusLeft );
          std::string iMouse = "iMouse[" + std::to_string(i) + "]";
          std::string iMouse1 = "iMouse1[" + std::to_string(i) + "]";
          glUniform2f(glGetUniformLocation(prog1, iMouse.c_str()), static_cast<float>(vec[i].rHand.first),
                         static_cast<float>(height_ - vec[i].rHand.second));
          glUniform2f(glGetUniformLocation(prog1, iMouse1.c_str()), static_cast<float>(vec[i].lHand.first),
                         static_cast<float>(height_ - vec[i].lHand.second));
     }
     shaderManager.get()->DrawElements();


     shaderManager.get()->UseProgramAndBindFramebuffer( prog2, framebuffer4 );
     shaderManager.get()->ClearBuffers();
     shaderManager.get()->SetTextureUniforms( texture2, texture3 );
     glUniform1i( glGetUniformLocation( prog2, "iChannel0" ), 0 );
     glUniform1i( glGetUniformLocation( prog2, "iChannel1" ), 1 );
     glUniform1ui( glGetUniformLocation( prog2, "mod" ), mod );
     glUniform2f( glGetUniformLocation( prog2, "iResolution" ), width_, height_ );
     shaderManager.get()->DrawElements();

     glUseProgram( prog );
     glBindFramebuffer( GL_FRAMEBUFFER, 0 );
     glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
     glClear( GL_COLOR_BUFFER_BIT );
     glActiveTexture( GL_TEXTURE0 + 0 );
     glBindTexture( GL_TEXTURE_2D, texture4 );
     glUniform2f( glGetUniformLocation( prog, "iResolution" ), width_, height_ );
     glUniform1i( glGetUniformLocation( prog, "iChannel0" ), 0 );

     glBindVertexArray( VAO );
     glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );

     shaderManager.get()->ProcessEvents();
}

GLFWwindow* SlimeMolding::getWindow()
{
     return shaderManager.get()->getWindow();
}


}// namespace touch_symphony