#ifndef FLUID_SIM_GAS_H
#define FLUID_SIM_GAS_H

#include <memory>

#include <shader_manager/shader_manager.h>

namespace touch_symphony
{


class FluidSimGas
{
private:
     GLuint prog;
     GLuint prog1;
     GLuint VAO;

     GLuint texture1, framebuffer1;
     GLuint texture2, framebuffer2;

     int width_, height_;
     std::shared_ptr< ShaderManager > shaderManager = nullptr;

public:
     FluidSimGas( int width, int height );
     ~FluidSimGas(){};
     int Init();
     void Draw( float x, float y, float time );
     GLFWwindow* getWindow();
};

}// namespace touch_symphony

#endif//FLUID_SIM_GAS
