#include "pseudo_particles.h"

#include <filesystem>
#include <iostream>

namespace fs = std::filesystem;
namespace touch_symphony
{

PseudoParticles::PseudoParticles( int width, int height ) : width_( width ), height_( height )
{

     shaderManager = std::make_shared< ShaderManager >( width, height );
}

int PseudoParticles::Init()
{
     std::string shaderFrag = ( fs::current_path() / "shaders" / "psuedo_particles" / "main.frag" ).string();
     std::string shaderFragBufferA = ( fs::current_path() / "shaders" / "psuedo_particles" / "buffer_a.frag" ).string();
     std::string shaderFragBufferB = ( fs::current_path() / "shaders" / "psuedo_particles" / "buffer_b.frag" ).string();
     std::string shaderVertex = ( fs::current_path() / "shaders" / "vertex.vert" ).string();

     prog = shaderManager.get()->CreateShaderProgram( shaderVertex, shaderFrag );
     prog1 = shaderManager.get()->CreateShaderProgram( shaderVertex, shaderFragBufferA );
     prog2 = shaderManager.get()->CreateShaderProgram( shaderVertex, shaderFragBufferB );
     VAO = shaderManager.get()->createVertexArrayObject();

     if( shaderManager.get()->createTextureAndBindFramebuffer( texture1, framebuffer1, width_, height_ ) > 0 )
     {
          std::cerr << "Framebuffer is not complete!" << std::endl;
          return 1;
     }
     if( shaderManager.get()->createTextureAndBindFramebuffer( texture2, framebuffer2, width_, height_ ) > 0 )
     {
          std::cerr << "Framebuffer is not complete!" << std::endl;
          return 1;
     }
     if( shaderManager.get()->createTextureAndBindFramebuffer( texture3, framebuffer3, width_, height_ ) > 0 )
     {
          std::cerr << "Framebuffer is not complete!" << std::endl;
          return 1;
     }

     return 0;
}

void PseudoParticles::Draw( std::vector< SkeletonInfo > vec, int frame )
{
    

     glfwGetFramebufferSize( shaderManager.get()->getWindow(), &width_, &height_ );

     shaderManager.get()->UseProgramAndBindFramebuffer( prog2, framebuffer3 );
     shaderManager.get()->ClearBuffers();
     for (int i = 0; i < vec.size(); ++i) {
           glUniform2f( glGetUniformLocation( prog2, "iMouse" ), static_cast< float >( vec[i].rHand.first ),
                  static_cast< float >( height_ - vec[i].rHand.second ) );
     }
     shaderManager.get()->DrawElements();


     shaderManager.get()->UseProgramAndBindFramebuffer( prog1, framebuffer1 );
     shaderManager.get()->ClearBuffers();
     shaderManager.get()->SetTextureUniforms( texture2, texture3 );
     glUniform1i( glGetUniformLocation( prog1, "iChannel0" ), 0 );
     glUniform1i( glGetUniformLocation( prog1, "iChannel2" ), 1 );
     glUniform2f( glGetUniformLocation( prog1, "iResolution" ), width_, height_ );
     glUniform1i( glGetUniformLocation( prog1, "iFrame" ), frame );
     for (int i = 0; i < vec.size(); ++i) {
     glUniform2f( glGetUniformLocation( prog1, "iMouse" ), static_cast< float >( vec[i].rHand.first ),
                  static_cast< float >( height_ - vec[i].rHand.second ) );
     }

     shaderManager.get()->DrawElements();


     shaderManager.get()->UseProgramAndBindFramebuffer( prog1, framebuffer2 );
     shaderManager.get()->ClearBuffers();
     shaderManager.get()->SetTextureUniforms( texture1, texture3 );
     glUniform1i( glGetUniformLocation( prog1, "iChannel0" ), 0 );
     glUniform1i( glGetUniformLocation( prog1, "iChannel2" ), 1 );
     glUniform2f( glGetUniformLocation( prog1, "iResolution" ), width_, height_ );
     glUniform1i( glGetUniformLocation( prog1, "iFrame" ), frame );
     for (int i = 0; i < vec.size(); ++i) {
     glUniform2f( glGetUniformLocation( prog1, "iMouse" ), static_cast< float >( vec[i].rHand.first),
                  static_cast< float >( height_ - vec[i].rHand.second) );
     }


     shaderManager.get()->DrawElements();


     glUseProgram( prog );
     glBindFramebuffer( GL_FRAMEBUFFER, 0 );
     shaderManager.get()->ClearBuffers();
     glActiveTexture( GL_TEXTURE0 + 0 );
     glBindTexture( GL_TEXTURE_2D, texture1 );
     glUniform2f( glGetUniformLocation( prog, "iResolution" ), width_, height_ );
     glUniform1i( glGetUniformLocation( prog, "iChannel0" ), 0 );

     glBindVertexArray( VAO );
     glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );

     shaderManager.get()->ProcessEvents();
}
GLFWwindow* PseudoParticles::getWindow()
{
     return shaderManager.get()->getWindow();
}

}// namespace touch_symphony