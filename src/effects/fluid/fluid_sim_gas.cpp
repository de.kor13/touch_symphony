#include "fluid_sim_gas.h"

#include <filesystem>
#include <iostream>

namespace fs = std::filesystem;
namespace touch_symphony
{


FluidSimGas::FluidSimGas( int width, int height ) : width_( width ), height_( height )
{

     shaderManager = std::make_shared< ShaderManager >( width, height );
}

int FluidSimGas::Init()
{
     std::string shaderFrag = ( fs::current_path() / "shaders" / "fluid_sim_gas" / "main.frag" ).string();
     std::string shaderFragBufferA = ( fs::current_path() / "shaders" / "fluid_sim_gas" / "simulation.frag" ).string();
     std::string shaderVertex = ( fs::current_path() / "shaders" / "vertex.vert" ).string();

     prog = shaderManager.get()->CreateShaderProgram( shaderVertex, shaderFrag );
     prog1 = shaderManager.get()->CreateShaderProgram( shaderVertex, shaderFragBufferA );
     VAO = shaderManager.get()->createVertexArrayObject();

     if( shaderManager.get()->createTextureAndBindFramebuffer( texture1, framebuffer1, width_, height_ ) > 0 )
     {
          std::cerr << "Framebuffer is not complete!" << std::endl;
          return 1;
     }
     if( shaderManager.get()->createTextureAndBindFramebuffer( texture2, framebuffer2, width_, height_ ) > 0 )
     {
          std::cerr << "Framebuffer is not complete!" << std::endl;
          return 1;
     }

     return 0;
}

void FluidSimGas::Draw( float x, float y, float time )
{
     glfwGetFramebufferSize( shaderManager.get()->getWindow(), &width_, &height_ );

     shaderManager.get()->UseProgramAndBindFramebuffer( prog1, framebuffer1 );
     shaderManager.get()->ClearBuffers();
     glActiveTexture( GL_TEXTURE0 + 0 );
     glBindTexture( GL_TEXTURE_2D, texture2 );
     glUniform1i( glGetUniformLocation( prog1, "iChannel0" ), 0 );
     glUniform2f( glGetUniformLocation( prog1, "iResolution" ), width_, height_ );
     glUniform1f( glGetUniformLocation( prog1, "iTime" ), time );
     glUniform2f( glGetUniformLocation( prog1, "iMouse" ), static_cast< float >( x ),
                  static_cast< float >( height_ - y ) );


     shaderManager.get()->DrawElements();

     shaderManager.get()->UseProgramAndBindFramebuffer( prog1, framebuffer2 );
     shaderManager.get()->ClearBuffers();
     glActiveTexture( GL_TEXTURE0 + 0 );
     glBindTexture( GL_TEXTURE_2D, texture1 );
     glUniform1i( glGetUniformLocation( prog1, "iChannel0" ), 0 );
     glUniform2f( glGetUniformLocation( prog1, "iResolution" ), width_, height_ );
     glUniform1f( glGetUniformLocation( prog1, "iTime" ), time );
     glUniform2f( glGetUniformLocation( prog1, "iMouse" ), static_cast< float >( x ),
                  static_cast< float >( height_ - y ) );


     shaderManager.get()->DrawElements();


     glUseProgram( prog );
     glBindFramebuffer( GL_FRAMEBUFFER, 0 );
     glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
     glClear( GL_COLOR_BUFFER_BIT );
     glActiveTexture( GL_TEXTURE0 + 0 );
     glBindTexture( GL_TEXTURE_2D, texture1 );
     glUniform2f( glGetUniformLocation( prog, "iResolution" ), width_, height_ );
     glUniform1i( glGetUniformLocation( prog, "iChannel0" ), 0 );

     glBindVertexArray( VAO );
     glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );

     shaderManager.get()->ProcessEvents();
}

GLFWwindow* FluidSimGas::getWindow()
{
     return shaderManager.get()->getWindow();
}


}// namespace touch_symphony