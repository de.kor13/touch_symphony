#ifndef PSEUDO_PARTICLES_H
#define PSEUDO_PARTICLES_H

#include <vector>

#include <shader_manager/shader_manager.h>
#include <hand_tracking/kinect_tracker.h>

#include <memory>

namespace touch_symphony
{
class PseudoParticles
{
private:
     GLuint prog;
     GLuint prog1;
     GLuint prog2;
     GLuint VAO;

     GLuint texture1, framebuffer1;
     GLuint texture2, framebuffer2;
     GLuint texture3, framebuffer3;

     int width_, height_;
     std::shared_ptr< ShaderManager > shaderManager = nullptr;


public:
     PseudoParticles( int width, int height );
     ~PseudoParticles(){};
     int Init();
     void Draw( std::vector< SkeletonInfo > vec, int frame );
     GLFWwindow* getWindow();
};
}// namespace touch_symphony

#endif// PSEUDO_PARTICLES_H