    #version 330 core
    
    out vec4 fragColor;
    uniform vec2 iResolution; 
    uniform sampler2D iChannel0;

    void main(  )
    {
        vec2 uv = gl_FragCoord.xy / iResolution.xy;

        fragColor =  vec4(texture(iChannel0, uv).r,texture(iChannel0, uv).g,texture(iChannel0, uv).b,texture(iChannel0, uv).a);
    }


