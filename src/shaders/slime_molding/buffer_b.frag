#version 330 core
uniform vec2 iResolution; 
uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform uint mod;


const float FadeAmt = 0.99; // 0.0-1.0
//color
vec3 hs(vec3 c, float s)
{
    vec3 m=vec3(cos(s),s=sin(s)*.5774,-s);
    return c*mat3(m+=(1.-m.x)/3.,m.zxy,m.yzx);
}


void main() 
{
	vec2 uv = gl_FragCoord.xy / iResolution.xy;
    vec3 p = vec3(1.5/iResolution.xy, 0.);
    
    vec4 pDot, pTrail;
    vec2 pDir = textureLod(iChannel0, uv, 0.0).rg;
    
    pDot = textureLod(iChannel0, uv, 0.0)*.5+.5;
    pTrail = textureLod(iChannel1, uv, 0.0);
    pTrail += textureLod(iChannel1, uv+p.xz, 0.0);
    pTrail += textureLod(iChannel1, uv-p.xz, 0.0);
    pTrail += textureLod(iChannel1, uv+p.zy, 0.0);
    pTrail += textureLod(iChannel1, uv-p.zy, 0.0);
    pTrail*=.2;
    
    // make this cell white if it has a nonzero vector length
    if(length(pDir)>0.)
    {
       	pTrail = pDot;
    }
    
    // trail effect
    pTrail = mix(pDot, pTrail,FadeAmt);
  if (mod == uint(0)) {
      pTrail.rgb = hs(pTrail.rgb, -.1);
  }
  if (mod == uint(1)) {
      pTrail.rgb = hs(pTrail.rgb, 0.0);
  }
  if (mod == uint(2)) {
      pTrail.rgb = hs(pTrail.rgb, 0.1);
  }
      
    
	gl_FragColor = max(vec4(0.), min(vec4(1.),pTrail));
}