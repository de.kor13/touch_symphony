#version 330 core
uniform vec2 iResolution; 
// uniform vec2 iMouse;
uniform vec2 iMouse[3];
uniform vec2 iMouse1[3];
uniform float radiusR;
uniform float radiusL;
uniform float sLen;

uniform float iTime;

uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
//uniform sampler2D iChannel2;


const float ParticleDensity = 1.0; // 0.0-1.0

float sensorLen = sLen;//0.5
float sensorAng = 0.5;
float rotAng = 0.5;


mat2 rotate(float _angle)
{
    return mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle));
}


vec2 sense (vec2 _uv, vec2 _pDir)
{

    //float r = plot(uv,cPos.xy,size);   //Draws location of part

    vec2 sensorL = _uv+(rotate(-sensorAng)*_pDir)*sensorLen; // location of sensor A
    vec2 sensorC = _uv+_pDir*sensorLen;
    vec2 sensorR = _uv+(rotate(sensorAng)*_pDir)*sensorLen;

    float senseL = textureLod(iChannel1, sensorL, 0.0).a;
    float senseC = textureLod(iChannel1, sensorC, 0.0).a;
    float senseR = textureLod(iChannel1, sensorR, 0.0).a;

    if(senseC > senseL && senseC > senseR)
    {

    }

    else if (senseC < senseL && senseC < senseR)
    {

       _pDir = vec2(-0.5,-0.5);// for debug
    }


    else if(senseL < senseR)
    {
      _pDir *= rotate(-rotAng);
     // _pDir = vec2(0.0,0.5);// for debug
    }

    else if(senseL > senseR)
    {
      _pDir *= rotate(rotAng);
    //  _pDir = vec2(0.5,0.0);// for debug
    }
    
    return _pDir;

}



float hash12(vec2 p)
{
    vec3 MOD3 = vec3(443.8975, 397.2973, 491.1871);
    vec3 p3 = fract(vec3(p.xyx) * MOD3);
    p3 += dot(p3, p3.yzx + 19.19);
    return fract((p3.x + p3.y) * p3.z);
}

void main()
{
    vec2 res = iResolution.xy;
    vec2 px = 1. / res;
    vec2 uv = gl_FragCoord.xy / res;
    
    vec4 buf[9];
    buf[0] = textureLod(iChannel0, uv, 0.0);
    buf[1] = textureLod(iChannel0, fract(uv-vec2(px.x, 0.)), 0.0);
    buf[2] = textureLod(iChannel0, fract(uv-vec2(-px.x, 0.)), 0.0);
    buf[3] = textureLod(iChannel0, fract(uv-vec2(0., px.y)), 0.0);
    buf[4] = textureLod(iChannel0, fract(uv-vec2(0., -px.y)), 0.0);
    buf[5] = textureLod(iChannel0, fract(uv-vec2(px.x, px.y)), 0.0);
    buf[6] = textureLod(iChannel0, fract(uv-vec2(-px.x, px.y)), 0.0);
    buf[7] = textureLod(iChannel0, fract(uv-vec2(px.x, -px.y)), 0.0);
    buf[8] = textureLod(iChannel0, fract(uv-vec2(-px.x, -px.y)), 0.0);
    
    // this cell's particle direction & position, if any
    vec2 pDir = buf[0].rg;
    vec2 pPos = buf[0].ba;
    
    
    // update this cell's particle position
    pPos = mod(pPos+pDir, res);
    
    
    // clear the current cell if its particle leaves it
    if(floor(pPos)!=floor(gl_FragCoord.xy)) {
        pDir = vec2(0.);
        pPos = vec2(0.);
    }
    
    // add up any incoming particles
    float ct = 0.;
    vec2 pDirAdd = vec2(0.);
    vec2 pPosAdd = vec2(0.);
    for(int i=1; i<9; i++) {
        vec2 pPosI = buf[i].ba;
        pPosI = mod(pPosI+buf[i].rg, res);
        if(floor(pPosI)==floor(gl_FragCoord.xy)) {
            pDirAdd += buf[i].rg;
            pPosAdd += pPosI;
            ct ++;
        }
    }
    
    // if particles were added up, average and transfer them to the current cell
    if(ct>0.)
    {
        pDir = normalize(pDirAdd / ct);   
    //    pDir = mix(pDir, textureLod(iChannel1, uv).rg*2.-1., 0.1 );  //trail follow
        
        pDir = sense(uv,pDir); //direction follow
            
        pPos = pPosAdd / ct;    
    }
    
    // first frame particle setup


    for (int i = 0; i < 3; ++i)
    {
        if( ( distance(iMouse[i].xy, gl_FragCoord.xy) < radiusR) )
            if(ParticleDensity>hash12(gl_FragCoord.xy/res)) {
                vec2 randXY =
                    vec2(
                        hash12(mod(uv+iTime/100.-4., 100.)),
                        hash12(mod(uv-iTime/100.-8., 100.))
                    );
                
                pDir = normalize(randXY-.5);
                pPos = gl_FragCoord.xy;
            }

        if( ( distance(iMouse1[i].xy, gl_FragCoord.xy) < radiusL) )
            if(ParticleDensity>hash12(gl_FragCoord.xy/res)) {
                vec2 randXY =
                    vec2(
                        hash12(mod(uv+iTime/100.-4., 100.)),
                        hash12(mod(uv-iTime/100.-8., 100.))
                    );
                
                pDir = normalize(randXY-.5);
                pPos = gl_FragCoord.xy;
            }
    }

    
    
    
   gl_FragColor = vec4(pDir.x,pDir.y, pPos.x,pPos.y);
}

