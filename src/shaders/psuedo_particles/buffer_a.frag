#version 330 core
uniform vec2 iResolution; 
uniform int iFrame;
uniform vec2 iMouse;
//uniform float iTime;

uniform sampler2D iChannel0;
uniform sampler2D iChannel2;

out vec4 fragColor;

/*
    Buffer A: 
    
    Track cell's mass and velocity.
    
    4 main things hapening.
    
    1. Add mass to cell proportional  to: (change in mass) * (dot(mass gradient direction, cell  velocity))
    2. Subtract mass from cell proportional to: (average mass of neighborhood) * (dot(mass gradient direction, cell  velocity))
    3. Apply acceleration to cell velocity: CellVelocity += (VelocityAverage - CellVelocity) 
    4. Apply a gravity ish force to cell velocity. This "de snakes" the particles and clumps them into points.
    
    x = mass
    zw = velocity
    
*/


 
// Neighborhood averages

#define R iResolution.xy
#define m vec2((iMouse.xy - .5*R) / R.y)
#define KEY(v,m) texelFetch(iChannel3, ivec2(v, m), 0).x
#define ss(a, b, t) smoothstep(a, b, t)

#define A(p) texelFetch(iChannel0,  ivec2(p), 0)
#define B(p) texelFetch(iChannel1,  ivec2(p), 0)
#define C(p) texelFetch(iChannel2,  ivec2(p), 0)
#define D(p) texelFetch(iChannel3,  ivec2(p), 0)


// Add the force coming from the side
#define SIDE_FORCE

// Constants
const vec2 UP = vec2(0., 1.);
const vec2 DOWN = vec2(0., -1.);
const vec2 LEFT = vec2(-1., 0.);
const vec2 RIGHT = vec2(1., 0.);
const vec2 UPL = vec2(-1., 1.);
const vec2 DOWNL = vec2(-1., -1.);
const vec2 UPR = vec2(1., 1.);
const vec2 DOWNR = vec2(1., -1.);



// https://www.shadertoy.com/view/4djSRW
// https://www.shadertoy.com/view/4djSRW
vec2 hash22(vec2 p){
	vec3 p3 = fract(vec3(p.xyx) * vec3(.1031, .1030, .0973));
    p3 += dot(p3, p3.yzx+33.33);
    return fract((p3.xx+p3.yz)*p3.zy);
}

float hash12(vec2 p){
	vec3 p3  = fract(vec3(p.xyx) * .1031);
    p3 += dot(p3, p3.yzx + 33.33);
    return fract((p3.x + p3.y) * p3.z);
}

 
// Neighborhood averages
vec4 Grad(vec2 u)
{
    float h = 1.;
    
    vec4 up = A((u + vec2(0., h)));
    vec4 down = A((u - vec2(0., h)));
    vec4 left = A((u - vec2(h, 0.)));
    vec4 right = A((u + vec2(h, 0.)));
    
    
    vec4 upl = A((u + UPL));
    vec4 downl = A((u + DOWNL));
    vec4 upr = A((u + UPR));
    vec4 downr = A((u + DOWNR));
    
    vec4 avg = (up+down+left+right+upr+downr+upl+downl) / 8.;
    return avg;
}

// Direction of most mass relative to cell
vec2 massGrad(vec2 u)
{
    float up = A((u + UP)).x;
    float down = A((u + DOWN)).x;
    float left = A((u + LEFT)).x;
    float right = A((u + RIGHT)).x;

    float upl = A((u + UPL)).x;
    float downl = A((u + DOWNL)).x;
    float upr = A((u + UPR)).x;
    float downr = A((u + DOWNR)).x;
    
    vec2 cm = vec2(0., 0.);
    cm += up*UP;
    cm += down*DOWN;
    cm += left*LEFT;
    cm += right*RIGHT;
    
    cm += upl*UPL;
    cm += downl*DOWNL;
    cm += upr*UPR;
    cm += downr*DOWNR;
    
    return cm;
}

void main(){
 vec2 uv = vec2(gl_FragCoord.xy - 0.5*R.xy)/R.y;
    
    // bA.x = mass, bA.zw = velocity
    vec4 bA = A(gl_FragCoord.xy);
    
    vec4 grad = Grad(gl_FragCoord.xy); // Gradient at point of cell
    vec2 vAvg = grad.zw; // Average velocity of surrounding cells
    vec2 acc = vAvg - bA.zw; // Cell acceleration
    acc = clamp(acc, -1., 1.);

    bA = mix(bA, grad, .02); // Substitute in a little bit of the average

    vec2 mg = massGrad(gl_FragCoord.xy); // Direction relative to current cell of most mass

    float massAvg = grad.x; // Average surrounding mass
    float massDif = massAvg - bA.x; // How fast the mass should be changing
    
    // Add and subtract mass from cell based on how much its moving in the direction of most mass
    // and how fast the mass is changing
    float dp = dot(mg, bA.zw); // How much the cell is moving toward the center of mass gradient
    bA.x -= dp*massAvg;
    bA.x += .999*dp*massDif;
    
    // Apply acceleration
    bA.zw += acc;
    
    // Apply a gravity ish force (clumps stuff into single points)
    float r = max(length(mg), 1.);
    float grav = -(3.5*massAvg*bA.x) / (r*r);
    bA.zw -= (mg)*grav;
    
    // Apply some friction
    bA.zw -= bA.zw*.0003;
    
    
    // Add stuff with mouse
   // if(iMouse.z > 0.){
       vec2 v = (C(gl_FragCoord.xy).xy - iMouse.xy-3.);
       if(length(v) < 100.){
        
            float d = length(iMouse.xy-gl_FragCoord.xy);
            float frc = .095*exp2(-d*.05);       
        
            bA.zw -= frc * v;    
            bA.x+=.09*frc;
        }
 //   }
   
    
    #ifdef SIDE_FORCE
    // Apple a small force coming from the left and add some mass
    float sf = .002*ss(R.x, 0., gl_FragCoord.x * 0.9);
    bA.zw += sf*vec2(1., 0.);
    bA.x += .06*sf;
    #endif
   
    // Inita
    if(iFrame < 8){
        bA = vec4(0);
        
        if(hash12(gl_FragCoord.xy*343. + 232.) < .1){
            bA.zw = 12.*(2.*hash22(gl_FragCoord.xy*543. + 332.) - 1.);
            bA.x = hash22(gl_FragCoord.xy*543. + 332.).x;
        }
    }
    
    
    bA.zw = clamp(bA.zw, -120., 120.);
    bA.x = clamp(bA.x, 0., 1.);
   
    
    fragColor = bA;
}