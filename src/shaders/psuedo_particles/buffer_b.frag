#version 330 core
uniform vec2 iMouse;

out vec4 fragColor;

void main()
{
    fragColor = vec4(iMouse.xy, 0., 0.);
}