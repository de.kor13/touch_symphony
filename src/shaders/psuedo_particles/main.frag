#version 330 core

uniform sampler2D iChannel0;
uniform vec2 iResolution; 

out vec4 fragColor;
/*
    By Cole Peterson (Plento)
    
    
    An experiment in making a ton of stuff that look like particles.
    Still some things to work out but so far Im happy.
    
    Cells store mass and velocity.
    
    Use the mouse to drag stuff around.
    
    Press A to zoom while using the mouse.
    
*/


#define R iResolution.xy
#define A(p) texelFetch(iChannel0,  ivec2(p), 0)

vec3 pal(float t){
    return 0.5+0.44*cos(vec3(1.4, 1.1, 1.4)*t + vec3(1.1, 6.1, 4.4) + .5);
}

// Main color
vec3 color(vec2 u, vec4 bA){
    float t = abs(bA.z*2.) + abs(bA.w*4.) + 3.6*length(bA.zw);
    vec3 col = vec3(clamp(bA.x, 0., 1.)) * pal(t*.2 + .3);
    return col * 3.6;
}



// glow effect
float glow(vec2 u){
    vec2 uv = u / R;
    float blur = 0.;

    const float N = 3.;
    
    for(float i = 0.; i < N; i++)
    {
        blur += texture(iChannel0, uv + vec2(i*.001, 0.), 1.1).x;
        blur += texture(iChannel0, uv - vec2(i*.001, 0.), 1.1).x;
    
        blur += texture(iChannel0, uv + vec2(0., i*.001), 1.1).x;
        blur += texture(iChannel0, uv - vec2(0., i*.001), 1.1).x;
    }
        
    return blur / N*4.;
}


void main( ){
    vec2 uv = gl_FragCoord.xy / R;
        

    vec4 bA = A(gl_FragCoord.xy);

    vec3 col = color(gl_FragCoord.xy, bA);

    float g = glow(gl_FragCoord.xy);
    
    col += 0.08*g*pal(1.5*length(bA.zw));
    
    fragColor = vec4(sqrt(clamp(col, 0.0, 1.0)), 1.0);
}
