#version 330 core
uniform vec2 iResolution; 
uniform vec2 iMouse;
uniform float iTime;
uniform sampler2D iChannel0;

out vec4 FragColor; 

vec4 SimulateFluid(sampler2D FluidTex, float K, float v, float Dt, float DensityInit, float VorticityAmount, vec2 ExternalForce, vec2 TexUV, vec2 TexSize)
{
    // prepare factor
    float CentralScale = 0.5;
    float S = K / Dt;
    
    // fetch neighbor fluid info
    vec4 FluidC = textureLod(FluidTex, TexUV/TexSize, 0.0);
    vec4 FluidR = textureLod(FluidTex, (TexUV + vec2(1., 0.))/TexSize, 0.0);
    vec4 FluidL = textureLod(FluidTex, (TexUV + vec2(-1., 0.))/TexSize, 0.0);
    vec4 FluidU = textureLod(FluidTex, (TexUV + vec2(0., 1.))/TexSize, 0.0);
    vec4 FluidD = textureLod(FluidTex, (TexUV + vec2(0., -1.))/TexSize, 0.0);
    
    // Dvelocity/Dx, Dvelocity/dy
    vec4 FluidDx = (FluidR - FluidL)*CentralScale;
    vec4 FluidDy = (FluidU - FluidD)*CentralScale;
    
    float DivVelocity = FluidDx.x + FluidDy.y;
    vec2 DelDensity = vec2(FluidDx.z, FluidDy.z);
    
    // Mass conservation
    float OldDensity = FluidC.z;
    float NewDensity = OldDensity - Dt * dot(vec3(DelDensity, DivVelocity), FluidC.xyz);
    NewDensity = clamp(NewDensity, 0.7f, 3.0f);
    
    // Momentum conservation
    // --- slove for the equation`s right hand side
    vec2 PresurePart = -(S * DelDensity / DensityInit);
    vec2 VelocityLaplacian = FluidR.xy + FluidL.xy + FluidU.xy + FluidD.xy - 4.0*FluidC.xy;
    vec2 ViscosityPart = v * VelocityLaplacian;
    
    // --- back trace for velocity
    vec2 BackTracePos = TexUV - Dt * FluidC.xy;
    vec4 AdvertData = textureLod(FluidTex, BackTracePos/TexSize, 0.0);
    vec2 NewVelocity = AdvertData.xy;
    NewVelocity += Dt * (ViscosityPart/NewDensity + PresurePart/NewDensity + ExternalForce/NewDensity - AdvertData.xy * vec2(FluidDx.x, FluidDy.y));
    NewVelocity = max(vec2(0), abs(NewVelocity)-1e-4)*sign(NewVelocity); //velocity decay
    
    // --- vorticity confinement
    float NewCurl = (FluidU.x - FluidD.x + FluidL.y - FluidR.y);
    vec2 Vort = vec2(abs(FluidD.w) - abs(FluidU.w), abs(FluidR.w) - abs(FluidL.w));
    float VortLength = length(Vort) + 1e-5;
    Vort *= VorticityAmount/VortLength;
    NewVelocity.xy += Dt * Vort * NewCurl;
    
    // Solve boundary
    vec2 UV = TexUV / TexSize;
    NewVelocity.y *= smoothstep(.5,.48,abs(UV.y-0.5)); //Boundaries
    NewVelocity.x *= smoothstep(.5,.48,abs(UV.x-0.5)); //Boundaries
    
    vec4 OutData = vec4(NewVelocity, NewDensity, NewCurl);
    // clamp for stable
    OutData = clamp(OutData, vec4(vec2(-10), 0.1 , -10.), vec4(vec2(10), 4.0 , 10.));

    return OutData;
}

float DotSelf(vec2 p)
{
    return dot(p,p);
}

vec2 point1(float t) 
{
    t *= 0.62;
    return vec2(0.4,0.5 + sin(t)*0.2);
}

void main( )
{
    float InitDensity = 1.;
    
    vec2 ExtForce = vec2(0.0, 0.0);
    
    // fill mouse presure
    vec2 Dir = gl_FragCoord.xy - iMouse.xy;
    float DisMouse = length(Dir);
    if(DisMouse < 10.0 )
    {
        Dir = normalize(Dir);
        ExtForce = 50. * Dir;
    }
    
    // fill other presure
    vec2 uv =  gl_FragCoord.xy/iResolution.xy;
    ExtForce.xy += 0.75*vec2(.0003, 0.00015)/(DotSelf(uv-point1(iTime))+0.0001);
    
    vec4 SimulateResult = SimulateFluid(iChannel0, 0.2, 0.55, 0.19, InitDensity, 0.3, ExtForce,  gl_FragCoord.xy, iResolution.xy);
    
    // // fill condition
    // if (iFrame < 20)
    // {
    //     SimulateResult = vec4(0., 0., InitDensity, 0.0);
    // }
    
    FragColor = SimulateResult;
}
