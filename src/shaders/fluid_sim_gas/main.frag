#version 330 core

uniform sampler2D iChannel0;
uniform vec2 iResolution; 

out vec4 FragColor; 

void main() 
{

    vec2 uv = gl_FragCoord.xy / iResolution.xy; 
    vec4 texColor = textureLod(iChannel0, uv, 0.0);
    FragColor = texColor;
    
}