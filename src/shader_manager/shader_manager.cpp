#include "shader_manager.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
namespace touch_symphony
{
static void FramebufferSizeCallback( GLFWwindow* window, int width, int height )
{
     glViewport( 0, 0, width, height );
}

ShaderManager::ShaderManager( int width, int height )
{
     // Инициализация GLFW
     if( !glfwInit() )
     {
          std::cerr << "Failed to initialize GLFW" << std::endl;
          return;
     }

     // Создание окна GLFW
     window = glfwCreateWindow( width, height, "OpenGL touch_sy", nullptr, nullptr );
     if( !window )
     {
          std::cerr << "Failed to create GLFW window" << std::endl;
          glfwTerminate();
          return;
     }
     glfwMakeContextCurrent( window );
     glfwSetFramebufferSizeCallback( window, FramebufferSizeCallback );

     // Инициализация GLEW
     if( glewInit() != GLEW_OK )
     {
          std::cerr << "Failed to initialize GLEW" << std::endl;
          glfwTerminate();
          return;
     }
}

ShaderManager::~ShaderManager()
{

     glDeleteVertexArrays( 1, &VAO );
     glDeleteBuffers( 1, &VBO );
     glDeleteBuffers( 1, &EBO );
     glfwTerminate();
}

GLFWwindow* ShaderManager::getWindow() const
{
     return window;
}


const char* ShaderManager::loadFile( const std::string& filePath )
{
     std::ifstream file( filePath );
     if( !file.is_open() )
     {
          std::cerr << "Failed to open file: " << filePath << std::endl;
          return nullptr;
     }

     std::stringstream buffer;
     buffer << file.rdbuf();
     std::string fileContent = buffer.str();

     // Создаем копию содержимого файла в динамически выделенном массиве символов
     char* contentCopy = new char[ fileContent.length() + 1 ];
     strcpy( contentCopy, fileContent.c_str() );

     return contentCopy;
}


GLuint ShaderManager::compileShader( GLenum type, const std::string& filePath )
{
     // Загрузка содержимого файла шейдера

     const char* sourcePtr = loadFile( filePath );
     // Создание и компиляция шейдера
     GLuint shader = glCreateShader( type );
     glShaderSource( shader, 1, &sourcePtr, nullptr );
     glCompileShader( shader );

     // Проверка на успешную компиляцию шейдера
     GLint success;
     glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
     if( !success )
     {
          char infoLog[ 512 ];
          glGetShaderInfoLog( shader, 512, nullptr, infoLog );
          std::cerr << "Shader compilation failed:\n" << infoLog << std::endl;
          return 0;
     }

     return shader;
}

GLuint ShaderManager::CreateShaderProgram( const std::string& vertexShaderPath, const std::string& fragmentShaderPath )
{
     GLuint vertexShader = compileShader( GL_VERTEX_SHADER, vertexShaderPath );
     GLuint fragmentShader = compileShader( GL_FRAGMENT_SHADER, fragmentShaderPath );

     // Создание шейдерной программы и привязка шейдеров
     GLuint shaderProgram = glCreateProgram();
     glAttachShader( shaderProgram, vertexShader );
     glAttachShader( shaderProgram, fragmentShader );
     glLinkProgram( shaderProgram );

     // Проверка на успешную привязку шейдеров
     GLint success;
     glGetProgramiv( shaderProgram, GL_LINK_STATUS, &success );
     if( !success )
     {
          char infoLog[ 512 ];
          glGetProgramInfoLog( shaderProgram, 512, nullptr, infoLog );
          std::cerr << "Shader program linking failed:\n" << infoLog << std::endl;
          return 0;
     }

     // Удаление ненужных шейдеров
     glDeleteShader( vertexShader );
     glDeleteShader( fragmentShader );


     return shaderProgram;
}
GLuint ShaderManager::createVertexArrayObject()
{
     // Определение вершин и индексов
     std::vector< float > vertices = {
          1.0f,  1.0f,  0.0f,// Верхний правый угол
          1.0f,  -1.0f, 0.0f,// Нижний правый угол
          -1.0f, -1.0f, 0.0f,// Нижний левый угол
          -1.0f, 1.0f,  0.0f // Верхний левый угол
     };

     std::vector< unsigned int > indices = {
          0, 1, 3,// Первый треугольник
          1, 2, 3 // Второй треугольник
     };

     GLuint VAO, VBO, EBO;
     glGenVertexArrays( 1, &VAO );
     glGenBuffers( 1, &VBO );
     glGenBuffers( 1, &EBO );

     glBindVertexArray( VAO );

     glBindBuffer( GL_ARRAY_BUFFER, VBO );
     glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof( float ), vertices.data(), GL_STATIC_DRAW );

     glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, EBO );
     glBufferData( GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof( unsigned int ), indices.data(), GL_STATIC_DRAW );

     // Устанавливаем указатели на атрибуты вершин
     glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( float ), ( void* )0 );
     glEnableVertexAttribArray( 0 );

     // Отвязываем VAO
     glBindBuffer( GL_ARRAY_BUFFER, 0 );
     glBindVertexArray( 0 );

     return VAO;
}

int ShaderManager::createTextureAndBindFramebuffer( GLuint& texture, GLuint& framebuffer, int width, int height )
{
     // Генерация текстуры
     glGenTextures( 1, &texture );
     glBindTexture( GL_TEXTURE_2D, texture );
     glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, 0 );
     glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
     glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );


     glGenFramebuffers( 1, &framebuffer );
     glBindFramebuffer( GL_FRAMEBUFFER, framebuffer );
     glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0 );

     if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
     {
          std::cerr << "Framebuffer is not complete!" << std::endl;
          return 1;
     }

     return 0;
}

GLuint ShaderManager::getVAO() const
{
     return VAO;
}

void ShaderManager::DrawElements()
{
     glBindVertexArray( VAO );
     glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );
}

void ShaderManager::ClearBuffers()
{
     glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
     glClear( GL_COLOR_BUFFER_BIT );
}

void ShaderManager::UseProgramAndBindFramebuffer( GLuint program, GLuint framebuffer )
{
     glUseProgram( program );
     glBindFramebuffer( GL_FRAMEBUFFER, framebuffer );
}

void ShaderManager::SetTextureUniforms( GLuint texture1, GLuint texture2 )
{
     glActiveTexture( GL_TEXTURE0 + 0 );
     glBindTexture( GL_TEXTURE_2D, texture1 );
     glActiveTexture( GL_TEXTURE0 + 1 );
     glBindTexture( GL_TEXTURE_2D, texture2 );
}

void ShaderManager::ProcessEvents()
{
     glfwSwapBuffers( window );
     glfwPollEvents();
}

}// namespace touch_symphony