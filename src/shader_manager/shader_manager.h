

#ifndef SHADER_MANAGER_H
#define SHADER_MANAGER_H

#include <string>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
namespace touch_symphony
{
class ShaderManager
{
public:
     ShaderManager( int width, int height );
     ~ShaderManager();

     const char* loadFile( const std::string& filePath );
     GLFWwindow* getWindow() const;
     GLuint getVAO() const;
     GLuint compileShader( GLenum type, const std::string& filePath );
     GLuint CreateShaderProgram( const std::string& vertexShaderPath, const std::string& fragmentShaderPath );
     GLuint createVertexArrayObject();
     int createTextureAndBindFramebuffer( GLuint& texture, GLuint& framebuffer, int width, int height );

     void DrawElements();
     void ClearBuffers();
     void UseProgramAndBindFramebuffer( GLuint program, GLuint framebuffer );
     void SetTextureUniforms( GLuint texture1, GLuint texture2 );
     void ProcessEvents();

private:
     GLFWwindow* window;
     GLuint VBO, VAO, EBO;
};
}// namespace touch_symphony

#endif// SHADER_MANAGER_H