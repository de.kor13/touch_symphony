#ifndef HAND_TRACKER_EXCEPTION_H
#define HAND_TRACKER_EXCEPTION_H

#include <stdexcept>

namespace touch_symphony
{
     class HandTrackerException : public std::runtime_error
     {
     public:
     explicit HandTrackerException(const std::string& msg) : std::runtime_error(msg) {}

     explicit HandTrackerException(const char* msg) : std::runtime_error(msg) {}
     };

} // !namespace cbdc_pki

#endif //HAND_TRACKER_EXCEPTION_H