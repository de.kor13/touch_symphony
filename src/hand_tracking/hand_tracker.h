#ifndef HANDTRACKER_H
#define HANDTRACKER_H

#include <Python.h>
#include <stdexcept>
#include <utility>

namespace touch_symphony
{
class HandTracker
{
public:
     HandTracker( int x, int y );
     ~HandTracker();

     std::pair< int, int > start();

private:
     PyObject* pModule_ = nullptr;
     PyObject* pClass_ = nullptr;
     PyObject* pInstance_ = nullptr;
     int scale_x_ = 640;
     int scale_y_ = 480;
};
}// namespace touch_symphony

#endif// HANDTRACKER_H
