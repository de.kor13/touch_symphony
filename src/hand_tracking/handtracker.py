import cv2
import mediapipe as mp

class HandTracker:
    def __init__(self):
        self.mp_hands = mp.solutions.hands
        self.hands = self.mp_hands.Hands(static_image_mode=False, max_num_hands=1, min_detection_confidence=0.2)

        # Инициализация видеопотока
        self.cap = cv2.VideoCapture(0)# 640x480
        self.cap.set(3, 640)  # Установка ширины кадра в 1920  1080 пикселей
        self.cap.set(4, 480)

    def start(self):
        
        # while True:
            ret, frame = self.cap.read()

            # Переворачиваем кадр по горизонтали
            frame = cv2.flip(frame, 1)

            # Преобразуем кадр в RGB
            image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

            # Обнаружение рук
            results = self.hands.process(image)
            cx,cy = 0, 0
            # Получение координат ключевых точек рук
            if results.multi_hand_landmarks:
                    for hand_landmarks in results.multi_hand_landmarks:
                        lm = hand_landmarks.landmark[15]  # Получаем 16-ю точку
                        h, w, c = frame.shape
                        cx, cy = int(lm.x * w), int(lm.y * h)                       
                        cv2.circle(frame, (cx, cy), 5, (255, 0, 0), cv2.FILLED)

            # Отображение кадра с отслеживанием рук
            cv2.imshow('Hand Tracking', frame)

            if cv2.waitKey(1) == ord('q'):
                print(cx,cy)
               # break          
            return cx,cy
