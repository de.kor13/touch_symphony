#ifndef KINECT_TRACKER_H
#define KINECT_TRACKER_H

#include <Kinect.h>

#include <cmath>
#include <utility>
#include <vector>

#include <gesture_recognizer/gesture_controller.h>


struct Point
{
     float x;
     float y;
};

struct SkeletonInfo
{
     std::pair< float, float > rHand;
     std::pair< float, float > lHand;
     int modColor = 0;
     float radiusLeft = 30.;
     float radiusRight = 30.;
     float sensorLen = 0.025;
     bool isLeftHandOpen;
     bool isRightHandOpen;
};

class KinectTracker
{
public:
     KinectTracker( int width, int height );
     ~KinectTracker();

     bool initialize();
     std::vector< SkeletonInfo > getHands();

private:
     void releaseResources( IBody* pBody[], IBodyFrame*& pBodyFrame );
     void recordHands( int& frameCounter, std::vector< SkeletonInfo >& hands,
                       std::vector< std::vector< SkeletonInfo > >& recordedHands );
     void updateHands( std::vector< SkeletonInfo >& hands, IBody* pBody, int width_, int height_ );

     IKinectSensor* pSensor;
     IBodyFrameSource* pBodySource;
     IBodyFrameReader* pBodyReader;
     int width_;
     int height_;

     int frameCounter = 0;                                    // Счетчик кадров
     std::vector< std::vector< SkeletonInfo > > recordedHands;// Для записи положений рук каждые 30 кадров
     GestureController gestureController;

};

#endif// KINECT_TRACKER_H