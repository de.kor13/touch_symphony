import cv2
import mediapipe as mp

def main():
    # Загрузка модели обнаружения позы
    mp_pose = mp.solutions.pose
    pose = mp_pose.Pose(static_image_mode=False, min_detection_confidence=0.5, min_tracking_confidence=0.5)

    # Создание объекта для рисования landmarks на изображении
    mp_drawing = mp.solutions.drawing_utils

    # Загрузка видеопотока с веб-камеры
    cap = cv2.VideoCapture(0)

    while cap.isOpened():
        success, image = cap.read()
        if not success:
            continue

        # Преобразование изображения в RGB
        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        # Обнаружение позы на изображении
        results = pose.process(image_rgb)

        # Рисуем найденные точки позы на изображении
        if results.pose_landmarks:
            mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS)

        # Вывод обработанного изображения
        cv2.imshow('Pose Detection', image)

        # Для выхода нажмите 'q'
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Освобождение ресурсов
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
