#include <hand_tracking/hand_tracker.h>
#include <hand_tracking/hand_tacker_exception.h>

#include <filesystem>


namespace touch_symphony
{
HandTracker::HandTracker( int x, int y )
    : pModule_( nullptr ), pClass_( nullptr ), pInstance_( nullptr ), scale_x_( x ), scale_y_( y )
{
     Py_Initialize();
     PyObject* sys_path = PySys_GetObject( "path" );
     std::filesystem::path currentDir( __FILE__ );
     currentDir.remove_filename();
     int appendResult = PyList_Append( sys_path, PyUnicode_FromString( currentDir.string().c_str() ) );
     if( appendResult < 0 )
     {

          throw HandTrackerException( "Failed to append path to sys.path list" );
     }

     pModule_ = PyImport_ImportModule( "handtracker" );
     if( pModule_ == NULL )
     {
          PyErr_Print();
          throw HandTrackerException( "Failed to import module" );
     }

     pClass_ = PyObject_GetAttrString( pModule_, "HandTracker" );
     if( pClass_ == NULL )
     {
          PyErr_Print();
          Py_DECREF( pModule_ );
          throw HandTrackerException( "Failed to get class reference" );
     }


     pInstance_ = PyObject_CallObject( pClass_, NULL );
     if( pInstance_ == NULL )
     {
          PyErr_Print();
          Py_DECREF( pClass_ );
          Py_DECREF( pModule_ );
          throw HandTrackerException( "Failed to create class instance" );
     }
}

HandTracker::~HandTracker()
{
     Py_XDECREF( pInstance_ );
     Py_XDECREF( pClass_ );
     Py_XDECREF( pModule_ );
     Py_Finalize();
}

std::pair< int, int > HandTracker::start()
{
     if( pInstance_ == NULL )
     {
          throw HandTrackerException( "Instance is not initialized" );
     }

     PyObject* pResult = PyObject_CallMethod( pInstance_, "start", NULL );
     if( pResult == NULL )
     {
          PyErr_Print();
          throw HandTrackerException( "Failed to call method 'start'" );
     }

     if( !PyTuple_Check( pResult ) || PyTuple_Size( pResult ) != 2 )
     {
          PyErr_Print();
          Py_DECREF( pResult );
          throw HandTrackerException( "Invalid result returned from 'start' method" );
     }

     PyObject* pCx = PyTuple_GetItem( pResult, 0 );
     PyObject* pCy = PyTuple_GetItem( pResult, 1 );

     if( !pCx || !pCy || !PyLong_Check( pCx ) || !PyLong_Check( pCy ) )
     {
          PyErr_Print();
          Py_DECREF( pResult );
          throw HandTrackerException( "Invalid data type returned from 'start' method" );
     }

     int cx = static_cast< int >( ( static_cast< double >( PyLong_AsLongLong( pCx ) ) / 640 ) * scale_x_ );
     int cy = static_cast< int >( ( static_cast< double >( PyLong_AsLongLong( pCy ) ) / 480 ) * scale_y_ );

     if( PyErr_Occurred() )
     {
          PyErr_Print();
          Py_DECREF( pResult );
          throw HandTrackerException( "Error converting Python objects to C++ types" );
     }

     Py_DECREF( pResult );

     return std::make_pair( cx, cy );
}

}// namespace touch_symphony