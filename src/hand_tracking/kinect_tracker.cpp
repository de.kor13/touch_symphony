
#include "kinect_tracker.h"
#include <iostream>



float distance( const Point& p1, const Point& p2 )
{
     return std::sqrt( std::pow( p2.x - p1.x, 2 ) + std::pow( p2.y - p1.y, 2 ) );
}
KinectTracker::KinectTracker( int width, int height )
    : pSensor( nullptr ), pBodySource( nullptr ), pBodyReader( nullptr ), width_( width ), height_( height )
{
}

KinectTracker::~KinectTracker()
{
     if( pBodyReader != nullptr )
     {
          pBodyReader->Release();
          pBodyReader = nullptr;
     }
     if( pBodySource != nullptr )
     {
          pBodySource->Release();
          pBodySource = nullptr;
     }
     if( pSensor != nullptr )
     {
          pSensor->Close();
          pSensor->Release();
          pSensor = nullptr;
     }
}
void GestureController_GestureRecognized( GestureController* sender, GestureEventArgs e )
{
     std::cout << "Gesture recognized: Type - " << static_cast< int >( e.GestureType ) << ", Tracking ID - "
               << e.TrackingID << std::endl;
}
bool KinectTracker::initialize()
{

     HRESULT hr = GetDefaultKinectSensor( &pSensor );
     if( FAILED( hr ) )
     {
          std::cerr << "Failed to initialize Kinect sensor" << std::endl;
          return false;
     }

     hr = pSensor->Open();
     if( FAILED( hr ) )
     {
          std::cerr << "Failed to open Kinect sensor" << std::endl;
          return false;
     }
// для глубины
    // IDepthFrameSource* pDepthSource;
    //     hr = pSensor->get_DepthFrameSource(&pDepthSource);
    //     if (FAILED(hr)) {
    //         // Обработка ошибки
    //     }

    //  unsigned short minDistance = 0.5; // в метрах
    //  unsigned short maxDistance = 4.0; // в метрах
    
    // hr = pDepthSource->get_DepthMinReliableDistance(&minDistance);
    // if (FAILED(hr)) {
    //               std::cerr << "get_DepthMinReliableDistance" << std::endl;
    //       return false;
    // }
    // hr = pDepthSource->get_DepthMaxReliableDistance(&maxDistance);
    // if (FAILED(hr)) {
    //               std::cerr << "et_DepthMaxReliableDistance" << std::endl;
    //       return false;
    // }
    // IDepthFrameReader* pDepthReader;
    // hr = pDepthSource->OpenReader(&pDepthReader);
    // if (FAILED(hr)) {
    //     std::cerr << "OpenReader(&pDepthReader);" << std::endl;
    // }


     hr = pSensor->get_BodyFrameSource( &pBodySource );
     if( FAILED( hr ) )
     {
          std::cerr << "Failed to get body frame source" << std::endl;
          return false;
     }

     hr = pBodySource->OpenReader( &pBodyReader );
     if( FAILED( hr ) )
     {
          std::cerr << "Failed to open body frame reader" << std::endl;
          return false;
     }
     gestureController.GestureRecognized = GestureController_GestureRecognized;
     

     return true;
}

static void updateHandState( IBody* pBody, SkeletonInfo& info )
{
     HandState leftHandState = HandState_Unknown;
     HandState rightHandState = HandState_Unknown;

     HRESULT hr = pBody->get_HandLeftState( &leftHandState );
     if( SUCCEEDED( hr ) )
     {
          switch( leftHandState )
          {
               case HandState_Closed:
                    info.radiusLeft = 0.;
                    break;
               case HandState_Open:
                    info.radiusLeft = 30.;
                    break;
               case HandState_Lasso:
                    info.radiusLeft = 40.;
                    break;
          }
     }

     hr = pBody->get_HandRightState( &rightHandState );
     if( SUCCEEDED( hr ) )
     {
          switch( rightHandState )
          {
               case HandState_Closed:
                    info.radiusRight = 0.;
                    break;
               case HandState_Open:
                    info.radiusRight = 30.;
                    break;
               case HandState_Lasso:
                    info.radiusRight = 40.;
                    break;
          }
     }
}


void KinectTracker::updateHands( std::vector< SkeletonInfo >& hands, IBody* pBody, int width_, int height_ )
{
     SkeletonInfo info;
     BOOLEAN bTracked = false;
     HRESULT hr = pBody->get_IsTracked( &bTracked );
     
     if( SUCCEEDED( hr ) && bTracked )
     {
          

          Joint joints[ JointType_Count ];
          hr = pBody->GetJoints( JointType_Count, joints );
          if( SUCCEEDED( hr ) )
          {
               info.rHand.first =  width_- ( joints[ JointType_WristRight ].Position.X + 1 ) / 2 * width_;
               info.rHand.second = height_ - ( ( joints[ JointType_WristRight ].Position.Y + 1 ) / 2 * height_ );

               info.lHand.first = width_ - ( joints[ JointType_WristLeft ].Position.X + 1 ) / 2 * width_;
               info.lHand.second = height_ - ( ( joints[ JointType_WristLeft ].Position.Y + 1 ) / 2 * height_ );

               unsigned long long trackingId;
               hr = pBody->get_TrackingId(&trackingId);
               if (SUCCEEDED(hr))
               {
                    Body body;
                    body.TrackingId = trackingId;
                    std::copy(joints, joints + JointType_Count, body.Joints);
                    gestureController.Update(body);
               }


               // float distanceRL =
               //      distance( { info.rHand.first, info.rHand.second }, { info.lHand.first, info.lHand.second } );
                    
               // if( distanceRL < 100 )
               // {
               //      info.sensorLen = 0.8;
               //      info.radiusRight = 80;
               //      info.radiusLeft = 80;
               // }
               // if( distanceRL > 1100 && distanceRL < 1700 )
               // {
               //      info.sensorLen = 0.1;
               //      info.radiusRight = 10;
               //      info.radiusLeft = 10;
               // }
              // updateHandState( pBody, info );
          }

          hands.push_back( info );
     }
}

void KinectTracker::recordHands( int& frameCounter, std::vector< SkeletonInfo >& hands,
                                 std::vector< std::vector< SkeletonInfo > >& recordedHands )
{
     if( frameCounter > 10 && hands.size() != 0 )
     {
          recordedHands.push_back( hands );
          if( recordedHands.size() == 5 )
          {
               for( int i = 0; i < hands.size(); ++i )
               {
                    float totalDistanceR = 0;
                    float totalDistanceL = 0;
                    for( int j = 0; j < recordedHands.size() - 1; ++j )
                    {
                         totalDistanceR += distance(
                              { recordedHands[ j ][ i ].rHand.first, recordedHands[ j ][ i ].rHand.second },
                              { recordedHands[ j + 1 ][ i ].rHand.first, recordedHands[ j + 1 ][ i ].rHand.second } );
                         totalDistanceL += distance(
                              { recordedHands[ j ][ i ].lHand.first, recordedHands[ j ][ i ].lHand.second },
                              { recordedHands[ j + 1 ][ i ].lHand.first, recordedHands[ j + 1 ][ i ].lHand.second } );
                    }

                    totalDistanceR /= ( recordedHands.size() );
                    totalDistanceL /= ( recordedHands.size() );

                    if( totalDistanceR > 100. )
                    {
                         hands[ i ].sensorLen = 0.8;
                         hands[ i ].radiusRight = 80;
                    }

                    if( totalDistanceL > 100. )
                    {
                         hands[ i ].sensorLen = 0.8;
                         hands[ i ].radiusLeft = 80;
                    }
               }
               recordedHands.clear();
          }
          frameCounter = 0;
     }
     frameCounter++;
}

std::vector< SkeletonInfo > KinectTracker::getHands()
{
     std::vector< SkeletonInfo > hands;
     IBodyFrame* pBodyFrame = nullptr;
     HRESULT hr = pBodyReader->AcquireLatestFrame( &pBodyFrame );
     if( SUCCEEDED( hr ) )
     {
          IBody* pBody[ BODY_COUNT ] = { 0 };
          hr = pBodyFrame->GetAndRefreshBodyData( BODY_COUNT, pBody );
          if( SUCCEEDED( hr ) )
          {
               for( int i = 0; i < BODY_COUNT; ++i )
               {
                    updateHands( hands, pBody[ i ], width_, height_ );
               }
          }
          releaseResources( pBody, pBodyFrame );
     }

    // recordHands( frameCounter, hands, recordedHands );
     return hands;
}

void KinectTracker::releaseResources( IBody* pBody[], IBodyFrame*& pBodyFrame )
{
     for( int i = 0; i < BODY_COUNT; ++i )
     {
          if( pBody[ i ] != nullptr )
          {
               pBody[ i ]->Release();
               pBody[ i ] = nullptr;
          }
     }
     if( pBodyFrame != nullptr )
     {
          pBodyFrame->Release();
          pBodyFrame = nullptr;
     }
}