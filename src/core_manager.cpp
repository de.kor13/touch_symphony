#include <core_manager.h>

namespace touch_symphony
{

CoreManager& CoreManager::GetCoreManager()
{
     static CoreManager instance;
     return instance;
}

std::shared_ptr< SlimeMolding > CoreManager::GetSlimeMolding( int x, int y )
{
     if( !slimeMolding )
     {
          slimeMolding = std::make_shared< SlimeMolding >( x, y );
     }
     return slimeMolding;
}

std::shared_ptr< FluidSimGas > CoreManager::GetFluidSimGas( int x, int y )
{
     if( !fluidSimGas )
     {
          fluidSimGas = std::make_shared< FluidSimGas >( x, y );
     }
     return fluidSimGas;
}

std::shared_ptr< HandTracker > CoreManager::GetHandTracker( int x, int y )
{
     if( !handTracker )
     {
          handTracker = std::make_shared< HandTracker >( x, y );
     }
     return handTracker;
}

std::shared_ptr< PseudoParticles > CoreManager::GetPseudoParticles( int x, int y )
{
     if( !pseudoParticles )
     {
          pseudoParticles = std::make_shared< PseudoParticles >( x, y );
     }
     return pseudoParticles;
}

std::shared_ptr< KinectTracker > CoreManager::GetKinectTracker( int x, int y )
{
     if( !kinectTracker )
     {
          kinectTracker = std::make_shared< KinectTracker >( x, y );
     }
     return kinectTracker;
}

}// namespace touch_symphony